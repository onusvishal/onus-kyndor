import React, { Component } from 'react';
import { View, StyleSheet, Image, Text, ScrollView,
   Switch, Platform, AppState, TextInput, Modal, TouchableOpacity,
   PermissionsAndroid, Alert } from 'react-native';
import { SafeAreaView } from 'react-navigation';
import { Icon } from 'native-base';
import { Actions } from 'react-native-router-flux';
import axios from 'axios';
import TagInput from 'react-native-tag-input';

import ContactList from './ContactList';
import CardChatGroupList from '../cards/CardChatGroupList';
import CardCustomChatGroupList from '../cards/CardCustomChatGroupList';
import { SEND_INVITATION_URL, chatGroup, API_TOKEN } from '../helper/Constant';

const array = [];
let selectedArray = [];
const array_Custom = [];
let selectedArray_Custom = [];

const inputProps = {
keyboardType: 'default',
placeholder: 'Enter Email',
autoFocus: true,
style: {
  fontSize: 14,
  marginVertical: Platform.OS == 'ios' ? 10 : -2,
},
};

class InviteMember extends Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: true,
      toggleSwitch: true,
      onChangeSwitch: false,
      appState: AppState.currentState,
      data: [],
      data_custom: [],
      tempdata: [],
      tempdata_custom: [],
      selectedData: [],
      selectedData_custom: [],
      email: '',
      tags: [],
      text: "",
    };
    this.handleSelect = this.handleSelect.bind(this);
    this.handleCustomSelect = this.handleCustomSelect.bind(this);
  }

  componentWillMount(){
      if (Platform.OS === 'ios') {
        } else {
            PermissionsAndroid.requestMultiple([PermissionsAndroid.PERMISSIONS.READ_CONTACTS, PermissionsAndroid.PERMISSIONS.WRITE_CONTACTS]).then((result) => {
            })
          }
  }

  searchMember(text){
    const searchValue = text.toLowerCase();
    let filter = this.state.tempdata;
    filter = filter.filter((item) => item.name.toLowerCase().includes(searchValue)).map(({ id, name, avatar, description }) => ({ id, name, avatar, description }));
    this.setState({ data: filter });
  }

  handleSelect(index,data) {
    var toRemove = [];
    if (array[index] === true) {
      array[index] = false;
      toRemove = data;
      var index = selectedArray.indexOf(toRemove);
      selectedArray.splice(index, 1);
    } else {
      array[index] = true;
      selectedArray.push(data);
    }
    this.setState({ selectedData: selectedArray});
  }

  renderChatGroupList() {
        return this.state.data.map((item, index) =>
            <CardChatGroupList
              key={`index-${index}`}
              items={item}
              index={index}
              array={array}
              handleSelect={this.handleSelect.bind(this)}
            />
        );
  }

  handleCustomSelect(index,data) {
    var toRemove = [];
    if (array_Custom[index] === true) {
      array_Custom[index] = false;
      toRemove = data;
      var index = selectedArray_Custom.indexOf(toRemove);
      selectedArray_Custom.splice(index, 1);
    } else {
      array_Custom[index] = true;
      selectedArray_Custom.push(data);
    }
    this.setState({ selectedData_custom: selectedArray_Custom});
  }

  renderCustomChatGroupList() {
        return this.state.data.map((item, index) =>
            <CardCustomChatGroupList
              key={`index-${index}`}
              items={item}
              index={index}
              array_Custom={array_Custom}
              handleCustomSelect={this.handleCustomSelect.bind(this)}
            />
        );
  }

  navigation(){
    this.props.navigation.navigate('ContactList');
  }

  sendInvitation(){
    let emailArray = this.state.tags;
    emailArray.map((item) => {
        const configure = { headers: { 'Content-Type': 'application/json', 'x-access-token': API_TOKEN } };
        const data = JSON.stringify({
          email: item,
          groupId: 1730,
          groupName: 'my group'
        });
        console.log('PARMA-->>>', data);
        console.log('URL-->>>', SEND_INVITATION_URL);
        axios.post(SEND_INVITATION_URL, data, configure)
        .then((response) => {
          if (response.data.success === true) {
              console.log('Send Success');
          } else {
            Alert.alert('Alert',response.data.result.error)
          }
        })
        .catch((error) => {
          console.log(`Error: ${error.response.data}`);
          this.setState({ message: `${error}`, loading: false });
        });
    });
  }

  sendInvitation0(){
    const configure = { headers: { 'Content-Type': 'application/json', 'x-access-token': API_TOKEN } };
    const data = JSON.stringify({
      email: this.state.email,
      groupId: 1730,
      groupName: 'my group'
    });
    console.log('PARMA-->>>', data);
    console.log('URL-->>>', SEND_INVITATION_URL);
    axios.post(SEND_INVITATION_URL, data, configure)
    .then((response) => {
      if (response.data.success === true) {
        Alert.alert(response.data.result.message)
      } else {
        Alert.alert('Alert',response.data.result.error)
      }
    })
    .catch((error) => {
      console.log(`Error: ${error.response.data}`);
      this.setState({ message: `${error}`, loading: false });
    });
  }

  onChangeTags = (tags) => {
    this.setState({ tags });
  }

  onChangeText = (text) => {
    this.setState({ text });

    const lastTyped = text.charAt(text.length - 1);
    const parseWhen = [',', ' ', ';', '\n'];

    if (parseWhen.indexOf(lastTyped) > -1) {
      this.setState({
        tags: [...this.state.tags, this.state.text],
        text: "",
      });
    }
  }

  labelExtractor = (tag) => tag;

  render() {
    const { flex, header, buttonText, txtText, txtTextNormal, txtText_Bold, txtText_Black_Bold, txt_Bold, row, txtView } = styles;
    const { goBack } = this.props.navigation;
    return (
      <SafeAreaView  style={{flex: 1 }}>
      <View style={{flex: 1, backgroundColor: '#f3f3f7' }}>

      <View style={[header, Platform.OS === 'ios' ?
        { marginTop: 0, } : { marginTop: 0 }]}
        >
          <View>
            <View style={{ height: 50, width:50,
            alignItems: 'center', justifyContent:'center' }}>
            <Icon
              type='Feather'
              name='arrow-left'
              style={{color: '#fff'}}
              onPress={() => goBack()}
            />
            </View>
          </View>

          <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }} >
          <Text style={{ color: '#FFFFFF',
              fontSize: 18,
              fontWeight: 'bold',
              margin: 18, }}>Invite Members</Text>
          </View>

          <View style={{ alignItems: 'center', justifyContent:'center', marginRight: 18, width: 20}}>

          </View>
        </View>

        <View style={{ flex:1, marginLeft: 20, marginRight: 20}}>

        <TouchableOpacity onPress={() => this.props.navigation.navigate('ContactListScreen')}>
        <View style={[txtView, {marginTop: 50, height: 50, justifyContent: 'center', alignItems: 'center', marginLeft: 30, marginRight: 30, borderRadius: 25}]}>
        <Text style={buttonText}>
          Invite From Contact
        </Text>
        </View>
        </TouchableOpacity>

        <View style={{ justifyContent: 'center', alignItems: 'center', marginTop: 20}}>
        <Text style={txtText}>
          Or
        </Text>
        </View>

        <View style={{ justifyContent: 'center', alignItems: 'center', marginTop: 40}}>
        <Text style={txtText_Black_Bold}>
          Invite via Email
        </Text>
        <Text style={{ color: '#c6c8d0', fontSize: 14 }}>Click '+' to invite upto 5 Members</Text>
        </View>

        <View style={{ height: 30 }}></View>
        <TagInput
            value={this.state.tags}
            onChange={this.onChangeTags}
            labelExtractor={this.labelExtractor}
            text={this.state.text}
            onChangeText={this.onChangeText}
            tagColor="grey"
            tagTextColor="white"
            inputProps={inputProps}
            maxHeight={75}
          />

          <View style={[txtView, {marginTop: 50, height: 50, justifyContent: 'center', alignItems: 'center', marginLeft: 30, marginRight: 30, borderRadius: 25}]}>
          <Text  onPress={() => this.sendInvitation()} style={buttonText}>
            Invite
          </Text>
          </View>

        <View>
          <View style={[{height: 30, justifyContent: 'center', alignItems: 'center', marginLeft: 30, marginRight: 30, borderRadius: 25}]}>
          </View>

          </View>
        </View>
     </View>
     </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  flex: {
    flex: 1,
    marginTop: 20,
  },
  header: {
    height: 60,
    flexDirection: 'row',
    backgroundColor: '#511fb2'
  },
  row: {
    flexDirection: 'row'
  },
  buttonText: {
    color: '#FFFFFF',
    fontSize: 16,
    fontWeight: 'bold',
  },
  txtText: {
    color: '#000000',
    fontSize: 16
  },
  txtText_Black_Bold: {
    color: '#000000',
    fontSize: 18,
    fontWeight: 'bold',
  },
  txtTextNormal: {
    color: '#383e53',
    fontSize: 16
  },
  txtText_Bold: {
    color: '#511fb2',
    fontSize: 16,
    fontWeight: 'bold',
    margin: 18,
  },
  txt_Bold: {
    color: '#FFFFFF',
    fontSize: 18,
    fontWeight: 'bold',
  },
  txtView: {
    backgroundColor: '#511fb2',
  },
});

export default InviteMember
