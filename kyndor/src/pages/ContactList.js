import React, { Component } from 'react';
import { View, StyleSheet, Image, Text, ScrollView,
   Switch, Platform, AppState, TextInput, Linking, AsyncStorage } from 'react-native';
import { Icon } from 'native-base';
import { Actions } from 'react-native-router-flux';
import Contacts from 'react-native-contacts';
import { SafeAreaView } from 'react-navigation';

import { Spinner } from '../common/Spinner';
import CardContactList from '../cards/CardContactList';
import { BACKGROUND_COLOR } from '../helper/Constant';
import { tempArray } from '../../App';

const array = [];
let selectedContactArray = [];
let selectedContact = [];
class ContactList extends Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: true,
      toggleSwitch: true,
      onChangeSwitch: false,
      appState: AppState.currentState,
      searchText: '',
      data: [],
      tempdata: [],
      isSearch: false,
      selectedData: [],
      selectedCnt: [],
      message: 'Your contacts are loading'
    };
    this.handleSelect = this.handleSelect.bind(this);
  }

  componentWillMount(){
    selectedContactArray = [];
    selectedContact = [];
    this.setState({ data: [], tempdata: [] });
    AsyncStorage.getItem("contactList").then((value) => {
      if (value) {
        console.log('contactList success');
        this.setState({ data: value, tempdata: value });
      } else {
        var temp = [];
        let temp2 = [];
              Contacts.getAll((err, contacts) => {
               this.setState({ loading: false});
               if (err === 'denied') {

               } else {
                 // console.log('Success', contacts);
                 temp = contacts
                 AsyncStorage.setItem('contactList', contacts);
                 this.setState({ data: temp, tempdata: temp });
               }
             });

            for (let i = 0; i < this.state.data.length; i++) {
                let dic = this.state.data[i];
                    array[dic.recordID] = false;
                }
      }
    }).done();
  }

  backButtonClick(){
    const { goBack } = this.props.navigation;
    selectedContactArray = [];
    selectedContact = [];
    for (let i = 0; i < this.state.data.length; i++) {
            let dic = this.state.data[i];
            array[dic.recordID] = false;
        }
    goBack();
  }

  searchMember(){
    this.setState({ loading: true});
    const searchValue = this.state.searchText.toLowerCase();
    let filter = this.state.tempdata;
      filter = filter.filter((item) => item.givenName.toLowerCase().includes(searchValue)).map(({ givenName, middleName, familyName, jobTitle }) =>
      {
        return { givenName, middleName, familyName, jobTitle };
      });
      this.setState({ data: filter },() => {
          setTimeout(function () {
            console.log('setTimeout.....');
            this.setState({ loading: false }, () => {
              console.log('Success.....');
            });
          }.bind(this), 1000);
          this.setState({ isSearch: true});
      });
  }

  clearSearch(){
    this.setState({ loading: true});
    this.setState({ data: this.state.tempdata },() => {
        this.setState({ isSearch: false, searchText: ''});
    });

    setTimeout(function () {
      console.log('setTimeout.....');
      this.setState({ loading: false}, () => {
        console.log('Success.....');
      });
    }.bind(this), 1000);
  }

  componentDidMount(){
    console.log('componentDidMount');
  }

  componentDidUpdate(){
    console.log('componentDidUpdate');
  }

  handleSelect(recordID,data) {
    console.log('select click', data);
    var toRemove = [];
    var toRemove0 = [];
    if (array[recordID] === true) {
      array[recordID] = false;
      toRemove = data;
      var index = selectedContactArray.indexOf(toRemove);
      selectedContactArray.splice(recordID, 1);

      toRemove0 = data.phoneNumbers[0].number;
      var index0 = selectedContact.indexOf(toRemove0);
      selectedContact.splice(index0, 1);
    } else {
      array[recordID] = true;
      selectedContactArray.push(data);
      selectedContact.push(data.phoneNumbers[0].number);
    }
    this.setState({ selectedData: selectedContactArray, selectedCnt: selectedContact});
  }

  renderContactList() {
    if (this.state.loading) {
        return (
          <View style={styles.center3}>
            <Text style={{ padding: 20, fontSize: 18 }}>{this.state.message}</Text>
            <Spinner size="large" />
          </View>
        );
      }
        return this.state.data.map((item, index) =>
            <CardContactList
              key={`index-${index}`}
              items={item}
              index={index}
              array={array}
              handleSelect={this.handleSelect.bind(this)}
            />
        );
  }

  render() {
    const { goBack } = this.props.navigation;
    const { flex, header, buttonText, txtText, txtTextNormal, txtText_Bold, row } = styles;
    let selectedContact = ''
    if (Platform.OS === 'ios') {
      selectedContact = `sms:/open?addresses=${this.state.selectedCnt}`;
    } else {
      selectedContact = `sms:${this.state.selectedCnt}`;
    }
    return (
      <SafeAreaView  style={{flex: 1 }}>
      <View style={{flex: 1, backgroundColor: '#FFFFFF' }}>

      <View style={header} >
          <View>
            <View style={{ height: 50, width:50,
            alignItems: 'center', justifyContent:'center' }}>
            <Icon
              type='Feather'
              name='arrow-left'
              style={{color: '#fff'}}
              onPress={() => this.backButtonClick()}
            />
            </View>
          </View>

          <View style={{ flex: 1 }} >
            <View style={{ flex:1, marginLeft: 10, marginRight: 10}}>
              <View style={{ flexDirection: 'row', height: 40, marginTop: 10, backgroundColor:'#6d3ccb', borderRadius: 20, }}>

              <View style={{ flex: 1, justifyContent: 'center'}}>
                <View style={{ marginTop: 5}}>
                  <TextInput
                    underlineColorAndroid='transparent'
                    style={{ height: 40, width: '100%', marginLeft: 5, color: '#FFFFFF' }}
                    placeholder="Search Members"
                    autoCapitalize="none"
                    autoCorrect={false}
                    placeholderTextColor="#FFFFFF"
                    value={this.state.searchText}
                    onChangeText={(value) => this.setState({ searchText: value })}
                  />
                </View>
              </View>

              <View style={{ width: 40,}}>
              <View style={{ height: 40, width:40,
              alignItems: 'center', justifyContent:'center',  }}>
              {
                this.state.isSearch ? <Icon
                  type='EvilIcons'
                  name='close'
                  style={{color: '#fff'}}
                  onPress={() => this.clearSearch()}
                />
                :
                <Icon
                  type='EvilIcons'
                  name='search'
                  style={{color: '#fff'}}
                  onPress={() => this.searchMember()}
                />
              }

            </View>
              </View>

                </View>
            </View>
          </View>

          <View>
          <Text onPress={() => Linking.openURL(selectedContact)} style={buttonText}>Done</Text>
          </View>
        </View>

        <View style={{ flex:1, marginLeft: 20, marginRight: 20}}>

        <Text style={[txtText, {marginTop: 20}]}>{'Invite members from contacts'}</Text>
        <ScrollView>
        {this.renderContactList()}
        </ScrollView>
        </View>

     </View>
     </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  flex: {
    flex: 1,
    marginTop: 20,
  },
  header: {
    height: 60,
    flexDirection: 'row',
    backgroundColor: '#511fb2'
  },
  row: {
    flexDirection: 'row'
  },
  buttonText: {
    margin: 18,
    color: '#FFFFFF',
    fontSize: 16,
  },
  txtText: {
    color: '#c6c8d0',
    fontSize: 16
  },
  txtTextNormal: {
    color: '#383e53',
    fontSize: 16
  },
  txtText_Bold: {
    color: '#c6c8d0',
    alignSelf: 'center',
    fontSize: 16,
    fontWeight: 'bold',
    margin: 18,
  },
  center3: {
    backgroundColor: '#FFF',
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center'
  },
});
export default ContactList;
