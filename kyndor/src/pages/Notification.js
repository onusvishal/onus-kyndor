import React, { Component } from 'react';
import { View, StyleSheet, Image, Text,
  ScrollView, Switch, Platform, AppState, TextInput, TouchableOpacity, PermissionsAndroid, Alert } from 'react-native';
import { Icon } from 'native-base';
import { Actions } from 'react-native-router-flux';
import axios from 'axios';
import LinearGradient from 'react-native-linear-gradient';
import { SafeAreaView } from 'react-navigation';

import CardNotificationList from '../cards/CardNotificationList';
import { GET_GROUPREQUEST_URL, GET_CHANNELREQUEST_URL, GET_GROUPUPDATES_URL, GET_CHANNELUPDATES_URL,
  GROUP_APPROVE_DECLINE_URL, CHANNEL_APPROVE_DECLINE_URL, group, API_TOKEN, notificationUser } from '../helper/Constant';

const arrayNotification = [];
export class Notification extends Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: true,
      appState: AppState.currentState,
      data: [],
    };
    this.handleApprove = this.handleApprove.bind(this);
    this.handleDecline = this.handleDecline.bind(this);
  }

  componentWillMount(){
    this.getNotification();
  }

  getNotification(){
    const configure = { headers: { 'Content-Type': 'application/x-www-form-urlencoded', 'x-access-token': API_TOKEN } };
    let temp1 = []
    // First API
    axios.get(GET_GROUPREQUEST_URL, configure)
    .then((response) => {
      if (response.data.success === true) {
        temp1 = response.data.result
        this.setState({ data: temp1});
        // Second API
        axios.get(GET_CHANNELREQUEST_URL, configure)
        .then((response) => {
          if (response.data.success === true) {
            response.data.result.map((item) => {
                temp1.push(item);
            });
            this.setState({ data: temp1});
            // Third API
            axios.get(GET_GROUPUPDATES_URL, configure)
            .then((response) => {
              if (response.data.success === true) {
                response.data.result.map((item) => {
                    temp1.push(item);
                });
                console.log('Third Response:', response.data.result);
                this.setState({ data: temp1});
                    // Forth API
                    axios.get(GET_CHANNELUPDATES_URL, configure)
                    .then((response) => {
                      if (response.data.success === true) {
                        response.data.result.map((item) => {
                            temp1.push(item);
                        });
                        this.setState({ data: temp1});
                      } else {
                        console.log('Error:-', response.data.message);
                      }
                    })
                    .catch((error) => {
                      console.log(`Error: ${error.response.data}`);
                    });

              } else {
                console.log('Error:-', response.data.message);
              }
            })
            .catch((error) => {
              console.log(`Error: ${error.response.data}`);
            });

          } else {
            console.log('Error:-', response.data.message);
          }
        })
        .catch((error) => {
          console.log(`Error: ${error.response.data}`);
        });

      } else {
        console.log('Error:-', response.data.message);
      }
    })
    .catch((error) => {
      console.log(`Error: ${error.response.data}`);
    });
  }

  handleApprove(data) {
    console.log('Approve', data);
    const configure = { headers: { 'Content-Type': 'application/json', 'x-access-token': API_TOKEN } };
    let subscription_id =  data.subscription_id;
    let channel_sub_id =  data.channel_sub_id;
    if (data.state === 0) {
      // For Group
      const data = JSON.stringify({
        subscriptionId: subscription_id,
        state: 1,
      });
      console.log('PARMA-->>>', data);
      console.log('URL-->>>', GROUP_APPROVE_DECLINE_URL);
      axios.post(GROUP_APPROVE_DECLINE_URL, data, configure)
      .then((response) => {
        console.log('Response-->>>', response);
        if (response.data.success === true) {
            this.getNotification();
        } else {
          Alert.alert(response.data.error);
        }
      })
      .catch((error) => {
        console.log(`Error: ${error.response.data}`);
        this.setState({ message: `${error}`, loading: false });
      });
    } else {
      const data = JSON.stringify({
        channel_sub_id: channel_sub_id,
        state: 1,
      });
      console.log('PARMA-->>>', data);
      console.log('URL-->>>', CHANNEL_APPROVE_DECLINE_URL);
      axios.post(CHANNEL_APPROVE_DECLINE_URL, data, configure)
      .then((response) => {
        console.log('Response-->>>', response);
        if (response.data.success === true) {
          this.getNotification();
        } else {
          Alert.alert(response.data.error);
        }
      })
      .catch((error) => {
        console.log(`Error: ${error.response.data}`);
        this.setState({ message: `${error}`, loading: false });
      });
    }
  }

  handleDecline(data){
    let subscription_id =  data.subscription_id;
    let channel_sub_id =  data.channel_sub_id;
    const configure = { headers: { 'Content-Type': 'application/json', 'x-access-token': API_TOKEN } };
    if (data.state === 0) {
      // For Group
      const data = JSON.stringify({
        subscriptionId: subscription_id,
        state: -1,
      });
      console.log('PARMA-->>>', data);
      console.log('URL-->>>', GROUP_APPROVE_DECLINE_URL);
      axios.post(GROUP_APPROVE_DECLINE_URL, data, configure)
      .then((response) => {
        console.log('Response-->>>', response);
        if (response.data.success === true) {
          this.getNotification();
        } else {
          Alert.alert(response.data.error);
        }
      })
      .catch((error) => {
        console.log(`Error: ${error.response.data}`);
        this.setState({ message: `${error}`, loading: false });
      });
    } else {
      const data = JSON.stringify({
        channel_sub_id: channel_sub_id,
        state: -1,
      });
      console.log('PARMA-->>>', data);
      console.log('URL-->>>', CHANNEL_APPROVE_DECLINE_URL);
      axios.post(CHANNEL_APPROVE_DECLINE_URL, data, configure)
      .then((response) => {
        console.log('Response-->>>', response);
        if (response.data.success === true) {
          this.getNotification();
        } else {
          Alert.alert(response.data.error);
        }
      })
      .catch((error) => {
        console.log(`Error: ${error.response.data}`);
        this.setState({ message: `${error}`, loading: false });
      });
    }
  }

  renderNotificationList() {
    console.log('195', this.state.data);
        return this.state.data.map((item, index) =>
            <CardNotificationList
              key={`index-${index}`}
              items={item}
              index={index}
              arrayNotification={arrayNotification}
              handleApprove={this.handleApprove.bind(this)}
              handleDecline={this.handleDecline.bind(this)}
            />
        );
  }

  render() {
    const { flex, header, buttonText, txtText, txtTextNormal, txtText_Bold, txt_Bold, row } = styles;
    return (
      <SafeAreaView  style={{flex: 1 }}>
      <View style={{flex: 1, backgroundColor: '#f3f3f7' }}>

      <View style={[header, Platform.OS === 'ios' ?
        { marginTop: 0, } : { marginTop: 0 }]}
        >
          <View>
            <View style={{ height: 50, width:50,
            alignItems: 'center', justifyContent:'center' }}>
            <Icon
              style={{color: '#fff'}}
            />
            </View>
          </View>

          <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }} >
          <Text style={{ color: '#FFFFFF',
              fontSize: 16,
              fontWeight: 'bold',
              margin: 18, }}>Notifications</Text>
          </View>

          <View>
          <Text style={[buttonText, {width: 20}]}></Text>
          </View>
        </View>

        <View style={{ flex:1, marginLeft: 20, marginRight: 20}}>

        <ScrollView>
        {this.renderNotificationList()}
        </ScrollView>

        </View>
     </View>
     </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  flex: {
    flex: 1,
    marginTop: 20,
  },

  header: {
    height: 60,
    flexDirection: 'row',
    backgroundColor: '#511fb2'
  },
  row: {
    flexDirection: 'row'
  },
  buttonText: {
    margin: 18,
    color: '#FFFFFF',
    fontSize: 16,
  },
  txtText: {
    color: '#c6c8d0',
    fontSize: 16
  },
  txtTextNormal: {
    color: '#383e53',
    fontSize: 16
  },
  txtText_Bold: {
    color: '#511fb2',
    fontSize: 16,
    fontWeight: 'bold',
    margin: 18,
  },
  txt_Bold: {
    color: '#212121',
    fontSize: 18,
    fontWeight: 'bold',
  },
  imageperson: {
    height: 30,
    width: 30,
  },

});

export default Notification;
