import React, { Component } from 'react';
import { View, Text, TouchableOpacity, Image } from 'react-native';
import { Icon } from 'native-base';
import { notificationUser } from '../helper/Constant';

class CardNotificationList extends Component {
  render() {
    const { index, handleApprove, handleDecline, arrayNotification, subscription_id } = this.props;
    const { id, username, updated_by_name, group_name, state, channel_sub_id, channel_name } = this.props.items;
    const { container, container0, container00, txtTextNormal, imageperson, txtTextBold, cardCss, cardCss0 } = styles;
    // console.log(this.props.items);
    return (
      <View key={`index-${id}`} style={ container}>

          <View
              style={[ state === 0 ? cardCss : cardCss0 ]}
          >
              <View style={ container0 }>
              <Image
                source={notificationUser} style={styles.imageperson}
                />
                <View style={{ height: 10 }} />
              </View>

              <View style={{ marginTop: 10, marginLeft: 10, marginRight: 5, flex: 1}}>
              {
                state === 0 ?
                <View>
                {
                  channel_sub_id === undefined ?
                  <Text style={txtTextBold}>{username} <Text style={{ fontWeight: 'normal',}}> wants to join </Text> {group_name}</Text>
                  :
                  <Text style={txtTextBold}>{username} <Text style={{ fontWeight: 'normal',}}> wants to join </Text> {channel_name} <Text style={{ fontWeight: 'normal',}}> of </Text>  {group_name}</Text>
                }
                <View style={{ flexDirection: 'row',
                alignItems: 'center'
                }}>
                  <View style={{
                    backgroundColor: '#FFFFFF',
                    borderRadius: 20,
                    borderWidth: 1,
                    borderColor: '#511fb2', height: 30, justifyContent: 'center', margin: 10}}>
                    <Text style={{ backgroundColor: '#FFFFFF', margin: 10, color: '#511fb2' }}
                    onPress={() => handleApprove(this.props.items)}
                    >Approve</Text>
                  </View>

                  <View style={{
                    backgroundColor: '#FFFFFF',
                    borderRadius: 20,
                    borderWidth: 1,
                    borderColor: '#d91717', height: 30, justifyContent: 'center', margin: 10}}>
                      <Text style={{ backgroundColor: '#FFFFFF', margin: 10, color: '#d91717' }}
                      onPress={() => handleDecline(this.props.items)}
                      >Decline</Text>
                    </View>
                </View>
                </View>
                :
                <View>
                {
                  channel_sub_id === undefined ?
                  <Text style={txtTextBold}>{username} <Text style={{ fontWeight: 'normal',}}> is accepted by </Text> {updated_by_name} <Text style={{ fontWeight: 'normal',}}> to the group </Text> {group_name}</Text>
                  :
                  <Text style={txtTextBold}>{username} <Text style={{ fontWeight: 'normal',}}> is accepted by </Text> {updated_by_name} <Text style={{ fontWeight: 'normal'}}> to the </Text> {channel_name} <Text style={{ fontWeight: 'normal'}}> of </Text> {group_name}</Text>
                }
                </View>
              }

              </View>

          </View>

      </View>
    );
  }
}

const styles = {
  container: {
    marginTop: 20,
    flex: 1,
    backgroundColor: '#f3f3f7',
  },
  container0: {
    borderRadius: 20,
    height: 40,
    width: 40,
    margin: 10,
    justifyContent: 'center',
    alignItems: 'center',
  },
  container00: {
    borderRadius: 20,
    height: 40,
    width: 40,
  },
  cardCss: {
    flexDirection: 'row',
    backgroundColor: '#FFFFFF',
    borderRadius: 5,
    shadowRadius: 1,
    shadowOpacity: 0.3,
    shadowColor: '#c6c8d0',
    justifyContent: 'center',
    alignItems: 'center',
    borderLeftWidth: 5,
    borderLeftColor: '#511fb2'
  },
  cardCss0: {
    flexDirection: 'row',
    backgroundColor: '#FFFFFF',
    borderRadius: 5,
    shadowRadius: 1,
    shadowOpacity: 0.3,
    shadowColor: '#c6c8d0',
    justifyContent: 'center',
    alignItems: 'center',
  },
  txtTextNormal: {
    color: '#383e53',
    fontSize: 16
  },
  txtTextBold: {
    color: '#383e53',
    fontSize: 16,
    fontWeight: 'bold',
  },
  imageperson: {
    height: 40,
    width: 40,
    },
};

export default CardNotificationList;
