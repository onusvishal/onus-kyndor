import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View, AppState, StatusBar
} from 'react-native';
import { Root } from 'native-base';
import { createStackNavigator } from 'react-navigation';

import ContactList from "./src/pages/ContactList";
import CreateChatGroup from "./src/pages/CreateChatGroup";
import InviteMember from "./src/pages/InviteMember";
import MemberList from "./src/pages/MemberList";
import SubGroupList from "./src/pages/SubGroupList";
import Notification from "./src/pages/Notification";

const AppNavigator = createStackNavigator({
  InviteMemberScreen: { screen: InviteMember },
  CreateChatGroupScreen: { screen: CreateChatGroup },
  NotificationScreen: { screen: Notification },
  SubGroupListScreen: { screen: SubGroupList },
  ContactListScreen: { screen: ContactList },
  MemberListScreen: { screen: MemberList },
},
{
  headerMode: 'none'
}
);

export default class App extends Component {
  render() {
    return (
      <AppNavigator />
    );
  }
}
