this.setState({ loading: true});
const searchValue = text.toLowerCase();
let filter = this.state.tempdata;
filter = filter.filter((item) => item.givenName.toLowerCase().includes(searchValue)).map(({ givenName, middleName, familyName, jobTitle }) => ({ givenName, middleName, familyName, jobTitle }));
this.setState({ data: filter });
